<?php
$time_offset = (int)dna_site_variable_get($node->site_id, 'time_offset');
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="canonical" href="<?php print $canonical_url; ?>">
  <meta property="fb:article_style" content="DNAinfo">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<header>
  <h1><?php print $node->field_headline[$node->language][0]['value'];?></h1>
  <!-- A kicker for the article -->
  <?php if (!empty($kicker)): ?>
  <h3 class="op-kicker">
    <?php print $kicker; ?>
  </h3>
  <?php endif; ?>
  <!-- The cover image shown inside your article -->
  <?php if (!empty($header_image['url'])): ?>
  <figure>
    <img src="<?php print $header_image['url'];?>" />
    <?php if (!empty($header_image['caption'])): ?>
    <figcaption><?php print $header_image['caption'];?></figcaption>
    <?php endif; ?>
  </figure>
  <?php endif; ?>
  <time class="op-published" dateTime="<?php print date(DATE_ATOM, ($node->field_published_date[$node->language][0]['value'] - $time_offset));?>"><?php print date('F jS, g:i A', ($node->field_published_date[$node->language][0]['value'] - $time_offset));?></time>
  <time class="op-modified" dateTime="<?php print date(DATE_ATOM, ($node->field_last_update[$node->language][0]['value'] - $time_offset));?>"><?php print date('F jS, g:i A', ($node->field_last_update[$node->language][0]['value'] - $time_offset));?></time>
  <?php if (!empty($analytics_metadata['authors'])): foreach ($analytics_metadata['authors'] as $name): ?>
  <address>
    <a><?php print $name?></a>
  </address>
  <?php endforeach; endif; ?>
  <?php if (!empty($settings['ad_url'])): ?>
    <!-- Ad code -->
  <figure class="op-ad">
    <iframe width="300" height="250" style="border:0; margin:0;" src="<?php print $settings['ad_url'];?>"></iframe>
  </figure>
  <?php endif; ?>
</header>
<?php print $node->body[$node->language][0]['value'];?>

<?php if (!empty($settings['analytics_config'])): ?>
  <!-- GA tracking code -->
  <figure class="op-tracker">
    <iframe>
      <script type="text/javascript">
        // GA Universal code.
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        <?php foreach($settings['analytics_config']['gau'] as $key => $gau_account): ?>
        ga('create', '<?php print $gau_account;?>', 'auto', '<?php print $key; ?>');
        // Set Hood.
        ga('<?php print $key; ?>.set', 'dimension2', '<?php print $analytics_metadata['hood'];?>');
        // Set author and author list.
        <?php if (!empty($analytics_metadata['authors'])): ?>
        // Set author.
        ga('<?php print $key; ?>.set', 'dimension3', '<?php print str_replace("'", "\\'", $analytics_metadata['authors'][0]);?>');
        // Set author list.
        ga('<?php print $key; ?>.set', 'dimension5', '<?php print str_replace("'", "\\'", implode('|', $analytics_metadata['authors']));?>');
        <?php endif; ?>
        // Set city.
        ga('<?php print $key; ?>.set', 'dimension6', '<?php print $analytics_metadata['city'];?>');
        // Set grouphood.
        ga('<?php print $key; ?>.set', 'dimension8', '<?php print $analytics_metadata['grouphood'];?>');
        // Set topics.
        ga('<?php print $key; ?>.set', 'dimension9', '<?php print $analytics_metadata['topics'];?>');
        // Set tags.
        ga('<?php print $key; ?>.set', 'dimension10', '<?php print $analytics_metadata['tags'];?>');
        // Set source_type.
        ga('<?php print $key; ?>.set', 'dimension11', 'fbia');
        // Set Source/Medium
        ga('<?php print $key; ?>.require', 'displayfeatures');
        ga('<?php print $key; ?>.set', 'campaignSource', 'Facebook');
        ga('<?php print $key; ?>.set', 'campaignMedium', 'social');
        // Send page view hit.
        ga('<?php print $key; ?>.send', 'pageview', {'page': '<?php print ltrim($analytics_metadata['canonical_url_raw'], '/ ');?>', 'title': '<?php print $analytics_metadata['title'];?>'});
        <?php endforeach; ?>
      </script>
      <script>
        // Parsely script.
        PARSELY = {
          autotrack: false,
          onload: function() {
            PARSELY.beacon.trackPageView({
              urlref: 'http://facebook.com/instantarticles'
            });
            return true;
          }
        }
      </script>
      <div id="parsely-root" style="display: none">
        <span id="parsely-cfg" data-parsely-site="<?php print $settings['analytics_config']['parsely_domain'];?>"></span>
      </div>
      <script>
        (function(s, p, d) {
          var h=d.location.protocol, i=p+"-"+s,
            e=d.getElementById(i), r=d.getElementById(p+"-root"),
            u=h==="https:"?"d1z2jf7jlzjs58.cloudfront.net"
              :"static."+p+".com";
          if (e) return;
          e = d.createElement(s); e.id = i; e.async = true;
          e.src = h+"//"+u+"/p.js"; r.appendChild(e);
        })("script", "parsely", document);
      </script>
    </iframe>
  </figure>
<?php endif; ?>
</body>
</html>
