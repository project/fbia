<?php

/**
 * Implements settings form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function fbia_settings_form($form, &$form_state) {
  $settings = variable_get('fbia', NULL);
  $settings['development_mode'] = variable_get('fbia_development_mode', 'yes');
  $form['app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App id'),
    '#description' => t('Type in your facebook app id'),
    '#default_value' => (!empty($settings['app_id'])) ? $settings['app_id'] : '',
    '#required' => TRUE,
  );
  $form['app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#description' => t('Type in the facebook app secret.'),
    '#default_value' => (!empty($settings['app_secret'])) ? $settings['app_secret'] : '',
    '#required' => TRUE,
  );
  $form['page_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Page id'),
    '#description' => t('Type in your facebook page id'),
    '#default_value' => (!empty($settings['page_id'])) ? $settings['page_id'] : '',
    '#required' => TRUE,
  );
  $form['access_token'] = array(
    '#type' => 'textarea',
    '#title' => t('Access token'),
    '#description' => t('Type in the facebook permanent access token for your page.'),
    '#default_value' => (!empty($settings['access_token'])) ? $settings['access_token'] : '',
    '#required' => TRUE,
  );
  $form['publishing_method'] = array(
    '#type' => 'radios',
    '#title' => t('Publishing method'),
    '#description' => t("Asynchronous method is fast and you don't have to wait for the call to facebook to finish."),
    '#default_value' => (!empty($settings['publishing_method'])) ? $settings['publishing_method'] : 'synchronous',
    '#options' => array('synchronous' => t('Synchronous'), 'asynchronous' => t('Asynchronous')),
    '#required' => TRUE,
  );
  $form['development_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Development mode'),
    '#description' => t('Are you not ready for production mode?.'),
    '#default_value' => (!empty($settings['development_mode'])) ? $settings['development_mode'] : 'yes',
    '#options' => array('yes' => t('Yes'), 'no' => t('No')),
    '#required' => TRUE,
  );
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content types'),
    '#options' => drupal_map_assoc(array_keys(node_type_get_types())),
    '#description' => t('Content types that allowed to send their content to the facebook instant article.'),
    '#default_value' => (!empty($settings['content_types'])) ? $settings['content_types'] : array(),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Settings form submit handler.
 * @param $form
 * @param $form_state
 */
function fbia_settings_form_submit($form, &$form_state) {
  $fbia = array(
    'app_id' => $form_state['values']['app_id'],
    'app_secret' => $form_state['values']['app_secret'],
    'page_id' => $form_state['values']['page_id'],
    'access_token' => $form_state['values']['access_token'],
    'publishing_method' => $form_state['values']['publishing_method'],
    'content_types' => $form_state['values']['content_types'],
  );
  $development_mode = $form_state['values']['development_mode'];
  variable_set('fbia', $fbia);
  variable_set('fbia_development_mode', $development_mode);
  drupal_set_message(t('Configuration saved successfully!'));
}