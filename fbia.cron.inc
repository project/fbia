<?php
/**
 * Created by PhpStorm.
 * User: bappasarkar
 * Date: 7/20/16
 * Time: 11:36 AM
 */

/**
 * Callback for processing FBIA queue.
 */
function fbia_cron_callback() {
  // Only run if request is using correct cron key.
  // Use the same cron key as Drupal's cron key.
  if (!isset($_GET['cron_key']) || variable_get('cron_key', 'drupal') != $_GET['cron_key']) {
    watchdog('cron', 'FBIA cron could not run because an invalid key was used.', array(), WATCHDOG_NOTICE);
    drupal_access_denied();
    return;
  }
  elseif (variable_get('maintenance_mode', 0)) {
    watchdog('cron', 'Cron could not run because the site is in maintenance mode.', array(), WATCHDOG_NOTICE);
    drupal_access_denied();
    return;
  }

  // Try to acquire custom cron lock.
  if (!lock_acquire('cron_fbia')) {
    watchdog('fbia', 'FBIA cron processing is already in progress.', array(), WATCHDOG_NOTICE);
  }

  // Wait for a short amount of time to account for the duration it takes for
  // the initiating process to write the record to the queue table.
  // Otherwise, this process will not find any records in the queue table
  // and won't process a single item immediately.
  usleep(variable_get('fbia_async_process_wait', 1000) * 1000);

  // Make sure every queue exists. There is no harm in trying to recreate an existing queue.
  DrupalQueue::get('FBIAQueue')->createQueue();

  // Define logic to process queue items.
  // Logic very heavily borrowed from drupal_cron_run().
  $end = time() + FBIA_QUEUE_MAX_EXECUTION_TIME;
  $queue = DrupalQueue::get('FBIAQueue');

  // Begin fbia queue processing.
  watchdog('fbia', t('Begin processing FBIA queue items. Found !count item(s).', array(
    '!count' => $queue->numberOfItems(),
  )));

  // Iterate over items in the queue.
  while (time() < $end && ($item = $queue->claimItem())) {
    try {
      $data = $item->data;
      if ($data['op'] == 'publish') {
        call_user_func('_fbia_publish_instant_article', $data['data']);
      }
      else {
        call_user_func('_fbia_unpublish_instant_article', $data['data']);
      }
      $queue->deleteItem($item);
    }
    catch (Exception $e) {
      // Release cron lock.
      lock_release('cron_fbia');
      // In case of exception log it and leave the item in the queue
      // to be processed again later.
      watchdog_exception('fbia', $e);
    }
  }

  // Set the last time this cron was run.
  variable_set('fbia_cron_last', REQUEST_TIME);
  watchdog('fbia', 'FBIA cron run completed.', array(), WATCHDOG_NOTICE);

  // Release cron lock.
  lock_release('cron_fbia');

  // We are returning JSON, so tell the browser.
  drupal_add_http_header('Content-Type', 'application/json');
  echo drupal_json_encode(array('success' => true));
}