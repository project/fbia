<!DOCTYPE html>
<html>
<head>
    <link rel="canonical" href="<?php print url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE));?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<header>
    <h1><?php print $node->title;?></h1>
    <time class="op-published" dateTime="<?php print date(DATE_ATOM, time());?>"><?php print date('F jS, g:i A', time());?></time>
    <time class="op-modified" dateTime="<?php print date(DATE_ATOM, $node->changed);?>"><?php print date('F jS, g:i A', $node->changed);?></time>
</header>
<?php print $node->body[$node->language][0]['value'];?>
</body>
</html>